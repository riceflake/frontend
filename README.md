# Libraries added:

- tailwindcss: css library that offer lot of helpers
- prettier: to format files, I did not added eslint due to not being familiar with typescript
- date-fns: format date more easily

# What has been done:

- Display a list of users, clicking on one user will fetch all the conversations from the selected user,
  ideally a website with login would be better and more secure since people would not be able to access to other people conversations
- Inside a conversation, there is a list of all the messages between two users.
- As a user, you can type and send new messages in a conversation
- Mobile responsive
- No tests has been added
- No bonus has been done

While I liked this exercise, I don't have enough time to do everything, I have other home test from other companies as well to do with a deadline

## Workflow

![](./workflow/workflow.gif)

### Points of improvements if I would have more hours to dedicated time on it

- Handling errors like a user that is not existing (i.e: http://localhost:3000/conversations/999) or conversation/users not existing in http://localhost:3000/messages/1?senderId=1&receiptId=2
- Handling error when api is sending error to the frontend when making a request
- Obviously adding test with jest / cypress for end-to-end
- Adding a "Back button" instead of clicking to the native browser previous button when navigating between users/conversations/messages
- Enforcing typescript which is barely the case in my test
- Bonus 1 and 2
- Show an "empty message" from conversations if it's empty
- Accessibility to blind people 

### How would I implemented bonus 1 ?

On the converstions page I would added a button "Start a conversation" with a kind of autocomplete input for selecting an user among all existing users. When cliking on "Start a conversation" it will create a conversation between these 2 users and would redirect to the messages page.
