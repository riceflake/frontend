const Avatar = ({ nickname }) => {
  const avatarColors = [
    'bg-red-500',
    'bg-yellow-500',
    'bg-green-500',
    'bg-blue-500',
    'bg-indigo-500',
    'bg-purple-500',
    'bg-pink-500',
  ];
  const randomColor = avatarColors[Math.floor(Math.random() * avatarColors.length)];
  return <div className={`rounded-full capitalize flex h-12 w-12 items-center justify-center ${randomColor}`}>{nickname.charAt(0)}</div>;
};

export default Avatar;
