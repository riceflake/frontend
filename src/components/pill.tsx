import PropTypes from 'prop-types';

const Bubble = ({ children, type }) => {
  return (
    <div className={
      `w-40 sm:w-80 m-4 py-2 px-4 
      flex items-center rounded-2xl
      ${type === 'default' && 'border-gray-500 hover:border-yellow-500 border-2'} 
      ${type === 'primary' && 'bg-blue-500 text-white'}
      ${type === 'secondary' && 'bg-gray-200 text-black'}`
    }>
      {children}
    </div>
  )
}

Bubble.propTypes = {
  type: PropTypes.oneOf(['default', 'primary', 'secondary'])
};

Bubble.defaultProps = {
  type: 'default',
};

export default Bubble