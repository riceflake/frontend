import { useState, useEffect, useContext } from 'react';
import type { FC } from 'react';
import Head from 'next/head';
import { Message } from 'types/message';
import { useRouter } from 'next/router';
import UsersContext from 'contextes/users';
import { format } from 'date-fns';
import Pill from 'components/pill';

const Messages: FC = () => {
  const users = useContext(UsersContext);
  const router = useRouter();
  const { conversationId, senderId, receiptId } = router.query;
  const [messages, setMessages] = useState<Message[]>([]);
  const [message, setMessage] = useState('');

  // @ts-ignore
  const senderUser = users.find((user) => user.id === parseInt(senderId));
  // @ts-ignore
  const receiptUser = users.find((user) => user.id === parseInt(receiptId));
  const isButtonDisabled = message === '';

  async function fetchMessagesByConversationsId(conversationId): Promise<any> {
    const response = await fetch(`http://localhost:3005/messages/${conversationId}`);
    const result = await response.json();
    setMessages(result);
  }

  async function sendMessage(): Promise<any> {
    await fetch(`http://localhost:3005/messages/${conversationId}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        body: message,
        // @ts-ignore
        authorId: parseInt(senderId),
        conversationId,
        timestamp: Date.now(),
      }),
    });
  }

  const sendMessageAndRefresh = async () => {
    await sendMessage();
    setMessage('');
    fetchMessagesByConversationsId(conversationId);
  };

  useEffect(() => {
    conversationId && fetchMessagesByConversationsId(conversationId);
  }, [conversationId]);

  return (
    <div className="h-full flex flex-col">
      <Head>
        <title>Conversation with {receiptUser && receiptUser.nickname}</title>
        <meta name="description" content="Messages"></meta>
      </Head>
      <div className="h-5/6 flex flex-col items-center justify-center">
        <h1 className="text-5xl m-4">Conversation with {receiptUser && receiptUser.nickname}</h1>
        <ul className="overflow-y-auto w-full flex flex-col">
          {messages.map((message) => {
            const isCurrentMessageFromSender = message.authorId === senderUser.id;
            return (
              <li
                key={message.id}
                title={format(new Date(message.timestamp * 1000), 'd MMMM Y Hmm')}
                className={`flex ${isCurrentMessageFromSender ? 'justify-end' : 'justify-start'}`}
              >
                <Pill type={isCurrentMessageFromSender ? 'primary' : 'secondary'}>
                  <span>{message.body}</span>
                </Pill>
              </li>
            );
          })}
        </ul>
      </div>
      <section className="h-1/6 flex items-center justify-center w-full">
          <input
            placeholder="Type your message here"
            className="m-4 p-4 h-12 w-9/12 border-gray-200 border-2 rounded-2xl"
            type="text"
            value={message}
            onKeyPress={(event) => {
              if (event.code === 'Enter' && !isButtonDisabled) {
                sendMessageAndRefresh();
              }
            }}
            onChange={(event) => setMessage(event.target.value)}
          />

          <button
            className={`bg-blue-500 text-white px-4 py-2 rounded ${isButtonDisabled && 'bg-blue-900'}`}
            disabled={isButtonDisabled}
            onClick={() => {
              sendMessageAndRefresh();
            }}
          >
            Send ✉️
          </button>
        </section>
    </div>
  );
};

export default Messages;
