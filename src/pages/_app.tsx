import type { AppProps } from 'next/app';
import { UsersProvider } from 'contextes/users';
import { getLoggedUserId } from '../utils/getLoggedUserId';
import '../styles/globals.css';

// Default way to get a logged user
export const loggedUserId = getLoggedUserId();

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <UsersProvider>
      <Component {...pageProps} />
    </UsersProvider>
  );
}

export default MyApp;
