import { useState, useEffect, useContext } from 'react';
import type { FC } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import UsersContext from 'contextes/users';
import Avatar from 'components/avatar';
import Pill from 'components/pill';

const Users: FC = () => {
  const users = useContext(UsersContext);

  return (
    <div className="h-full flex items-center justify-center">
      <Head>
        <title>Users</title>
        <meta name="description" content="Users"></meta>
      </Head>
      <div className="flex flex-col items-center">
        <h1 className="text-5xl m-4">User list</h1>
        <h5 className="m-4">click on a one of users below to see their list of conversation ⬇️</h5>
        <ul>
          {users.map((user) => (
            <Link key={user.id} href={`/conversations/${user.id}`} passHref>
              <a>
                <Pill>
                  <Avatar nickname={user.nickname} />
                  <span className="ml-4">{user.nickname}</span>
                </Pill>
              </a>
            </Link>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default Users;
