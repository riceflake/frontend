import { useState, useEffect } from 'react';
import type { FC } from 'react';
import Head from 'next/head';
import { Conversation } from 'types/conversation';
import { useRouter } from 'next/router';
import Link from 'next/link';
import Pill from 'components/pill';
import { format } from 'date-fns';

const Conversations: FC = () => {
  const router = useRouter();
  const { senderId } = router.query;
  const [conversations, setConversations] = useState<Conversation[]>([]);

  async function fetchConversationsByUserId(senderId): Promise<any> {
    const response = await fetch(`http://localhost:3005/conversations/${senderId}`);
    const result = await response.json();
    setConversations(result);
  }

  useEffect(() => {
    if (senderId) {
      fetchConversationsByUserId(senderId);
    }
  }, [senderId]);

  return (
    <div className="h-full flex items-center justify-center">
      <Head>
        <title>Conversations</title>
        <meta name="description" content="Conversations" />
      </Head>
      <div className="flex flex-col  items-center justify-center">
        <h1 className="text-5xl m-4">Conversations list </h1>
        <h5 className="m-4">click on a one of conversations below to see the list of messages ⬇️</h5>
        <ul>
          {conversations.map((conversation) => {
            return (
              <Link
                key={conversation.id}
                href={{
                  pathname: `/messages/${conversation.id}`,
                  query: {
                    senderId: conversation.senderId,
                    receiptId: conversation.recipientId,
                  },
                }}
                passHref
              >
                <a>
                  <Pill>
                    <span>
                      {conversation.recipientNickname} |{' '}
                      {format(new Date(conversation.lastMessageTimestamp * 1000), 'd MMMM Y')}
                    </span>
                  </Pill>
                </a>
              </Link>
            );
          })}
        </ul>
      </div>
    </div>
  );
};

export default Conversations;
