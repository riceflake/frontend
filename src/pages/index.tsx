import type { FC } from 'react';
import Users from 'pages/users';

const App: FC = () => {
  return (
      <Users />
  );
};

export default App;
