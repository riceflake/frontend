import { createContext, useState, useEffect } from 'react';

const UsersContext = createContext([]);

export const UsersProvider = ({ children }) => {
  const [users, setUsers] = useState([]);

  async function fetchUsers(): Promise<any> {
    const response = await fetch(`http://localhost:3005/users`);
    const result = await response.json();
    setUsers(result);
  }

  useEffect(() => {
    fetchUsers();
  }, []);

  return <UsersContext.Provider value={users}>{children}</UsersContext.Provider>;
};

export default UsersContext;
